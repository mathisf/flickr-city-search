$(document).ready(function() {
    // Initialisation
    var img;
    var dialog;
    var row;
    var imgHtml;
    var date;
    var table = $("#table").DataTable();

    //Navigation par onglet (tab)
    $(function() {
        $("#tabs").tabs();
    });

    //Autocompletion avec récuperation des villes (sur infoweb)
    $('#commune').keyup(function(event) { // keyup = quand l'utilisateur relache une touche
        $('#commune').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: 'http://infoweb-ens/~jacquin-c/codePostal/commune.php',
                    type: 'GET',
                    dataType: 'json',
                    data: 'commune=' + $("#commune").val() + '&maxRows=10',
                    success: function(dataReceived) {
                        response($.map(dataReceived, function(objet) {
                            return {
                                label: objet.Ville,
                                Ville: objet.Ville
                            };
                        }));
                    },
                    error: function() {
                        $('body').append("Erreur de requête (autocompletion)\n");
                    },
                });
            },
            minLength: 1,
            select: function(event, ui) {
                $('div').append(ui.item);
            }
        });
    });

    // fonction permettant d'ajouter une ligne et de mettre a jour le tableau
    function updateTab(img, title, date, owner) {
        table.row.add([
            img,
            title,
            date,
            owner
        ]).draw();
    }

    // Recherche de photos
    $('#button').on("click", function() {
        $('#images').html(""); // Vidage du premier onglet
        table.clear(); // Vidage du tableau

        var userInput = $('#commune').val().replace(/ /g, "+"); // La valeur du champ de texte
        var nbResults = $('#nb').val(); // Nombre de résultat (select)

        $.getJSON("https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=4eec50f813957e9e5f7be5286553b6bf&format=json&nojsoncallback=1&tags=" + userInput, function(data) {
            if (data.photos.total == 0 || data.stat === "fail") { // Si il n'y a pas de photo ou si il y a eu une erreur lors de la requete
                alert("Pas de résultat");
                return false;
            } else {
                $.each(data.photos.photo, function(i, photo) { // Pour chaque photo

                    if (i == nbResults) { // Quand on a atteint nbResults, on sort du for each
                        return false;
                    }

                    // Création des balises html
                    img = $("<img/>");
                    dialog = $("<div/>");
                    row = $("<tr/>");

                    // Création des images 
                    img.attr("src", "http:\/\/farm4.staticflickr.com\/" +
                        photo.server + "\/" + photo.id + "_" + photo.secret +
                        "_m.jpg").appendTo("#images");
                    img.appendTo("#images");

                    // Création des fenêtres modales 
                    dialog.attr("id", photo.id);
                    dialog.html("Nom : " + photo.title + "</br> id : " + photo.id);
                    dialog.appendTo("#images");

                    // Quand on clique sur une image, ça affiche la fenêtre modale de l'image voulue
                    img.on("click", function() {
                        $('#' + photo.id).dialog();
                    });

                    // On récupère la date de chaque image pour pouvoir inserer une ligne dans le tableau
                    $.getJSON("https://api.flickr.com/services/rest/?method=flickr.photos.getInfo&api_key=4eec50f813957e9e5f7be5286553b6bf&format=json&nojsoncallback=1&photo_id=" + photo.id, function(data) {
                        imgHtml = "<img src='http:\/\/farm4.staticflickr.com\/" + photo.server + "\/" + photo.id + "_" + photo.secret + "_m.jpg'>";

                        if (!(data.stat === "ok")) { // Si la requete a échoué
                            date = "Date inconnue";
                        } else {
                            date = data.photo.dates.taken;
                        }

                        updateTab(imgHtml, photo.title, date, photo.owner); // Ajout d'une ligne
                    });
                });
            }
        });
    });

});

//TODO Choisir une date pour chercher les photo de la requete